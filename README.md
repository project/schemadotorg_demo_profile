Table of contents
-----------------

* Introduction
* Features


Introduction
------------

The **Schema.org Blueprints Demo profile** installs commonly used features 
pre-configured for the Schema.org Blueprints Demo.

> A simplified version of Drupal's Standard profile which provides the ideal  
> user experience for demoing the Schema.org Blueprints module.

**THIS PROFILE IS A COPY OF DRUPAL'S STANDARD PROFILE WITH SOME MODULES AND 
CONFIGURATION REMOVED**


Features
--------

Changes from the Drupal's Standard installation profile.

- Removed the comment, contact, and tour modules
- Removed the article content types
- Removed the tags vocabulary
- Removed default image styles and responsive images
- Removed bartik and olivero configuration.
- Moved all /optional config to /install config.
- Updated tests to reflect changes.
- Post installation, switches installation profile to back to 'standard'.
